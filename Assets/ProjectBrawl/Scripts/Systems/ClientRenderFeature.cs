﻿
public class ClientRenderFeature : Feature {
    
    public ClientRenderFeature(Contexts contexts)
    {
        
        //Initialize
        Add(new TestInitSystem());
        Add(new PlayerRenderInitSystem(contexts.client));
        // Input
        Add(new MoveInputSystem(contexts.client));
        //Render
        Add(new PositionRenderSystem(contexts.client));
        //Cleanup
        Add(new ClientDisposeSystem(contexts.client));

    }
	
}
