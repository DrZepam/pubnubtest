﻿using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInputSystem : ReactiveSystem<ClientEntity> {

    private ClientContext _context;

    public MoveInputSystem(ClientContext context) : base(context)
    {
        _context = context;
    }

    protected override void Execute(List<ClientEntity> entities)
    {
        foreach(ClientEntity directionEntity in entities)
        {
            ClientEntity playerEntity = _context.GetEntityWithPlayer("0");
            if(playerEntity != null)
            {
                Vector3 dir = directionEntity.direction.Direction * Time.deltaTime;
                playerEntity.ReplacePosition(playerEntity.position.Position + dir);
            }
            directionEntity.isDispose = true;
        }
    }

    protected override bool Filter(ClientEntity entity)
    {
        return entity.hasDirection;
    }

    protected override ICollector<ClientEntity> GetTrigger(IContext<ClientEntity> context)
    {
        return context.CreateCollector(ClientMatcher.AllOf(ClientMatcher.Direction));
    }
}
