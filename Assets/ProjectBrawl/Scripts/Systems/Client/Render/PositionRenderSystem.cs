﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
public class PositionRenderSystem : IExecuteSystem {

    private ClientContext _context;
    private IGroup<ClientEntity> _positionEntities;

    public PositionRenderSystem(ClientContext context)
    {
        _context = context;
        _positionEntities = context.GetGroup(ClientMatcher.AllOf(ClientMatcher.Position,ClientMatcher.View));
    }

    public void Execute()
    {
        foreach(ClientEntity positionEntity in _positionEntities)
        {
            GameObject view = positionEntity.view.GameObject;
            view.transform.position = positionEntity.position.Position;
        }
    }


}
