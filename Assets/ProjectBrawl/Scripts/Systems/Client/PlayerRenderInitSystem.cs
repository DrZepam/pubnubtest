﻿using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRenderInitSystem : ReactiveSystem<ClientEntity> {

    private ClientContext _context;

    public PlayerRenderInitSystem(ClientContext context) : base(context)
    {
        _context = context;
    }

    protected override void Execute(List<ClientEntity> entities)
    {
        foreach(ClientEntity renderEntity in entities)
        {
            RenderPlayer(renderEntity);
        }
    }

    protected override bool Filter(ClientEntity entity)
    {
        return entity.hasPlayer;
    }

    protected override ICollector<ClientEntity> GetTrigger(IContext<ClientEntity> context)
    {
        return context.CreateCollector(ClientMatcher.Player);
    }

    #region Private Methods

    private void RenderPlayer(ClientEntity renderEntity)
    {
        // Instantiate GameObject
        var prefab = Resources.Load<GameObject>("Player");
        GameObject viewObject = Object.Instantiate(prefab);
        // Attach to view
        
        renderEntity.AddView(viewObject);
        renderEntity.AddPosition(Vector3.zero);
        renderEntity.isMovable = true;
    }
    #endregion


}
