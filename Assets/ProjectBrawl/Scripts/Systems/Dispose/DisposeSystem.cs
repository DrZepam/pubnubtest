﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
public class ClientDisposeSystem : ReactiveSystem<ClientEntity> {

    private ClientContext _context;

    public ClientDisposeSystem(ClientContext context) : base(context)
    {
        _context = context;
    }

    protected override void Execute(List<ClientEntity> entities)
    {
        foreach (ClientEntity disposeEntity in entities)
        {
            if (disposeEntity.isRemoveViewOnDispose)
            {
                if (disposeEntity.hasView)
                {
                    Object.Destroy(disposeEntity.view.GameObject);
                }
            }
            disposeEntity.Destroy();
        }
    }

    protected override bool Filter(ClientEntity entity)
    {
        return entity.isDispose;
    }

    protected override ICollector<ClientEntity> GetTrigger(IContext<ClientEntity> context)
    {
        return context.CreateCollector(ClientMatcher.Dispose);
    }
}
