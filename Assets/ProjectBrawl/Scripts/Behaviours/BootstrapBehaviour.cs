﻿using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootstrapBehaviour : MonoBehaviour {

    public Systems _systems;

    private void Awake()
    {
        InitSystems();
    }
    // Use this for initialization
    void Start () {
        _systems.Initialize();
	}
	
	// Update is called once per frame
	void Update () {
        _systems.Execute();
        _systems.Cleanup();
    }

    #region Private Methods
    private void InitSystems()
    {
        _systems = new ClientRenderFeature(Contexts.sharedInstance);
    }
    #endregion 
}
