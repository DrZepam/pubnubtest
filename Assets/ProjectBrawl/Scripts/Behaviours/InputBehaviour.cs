﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class InputBehaviour : MonoBehaviour {

    Contexts _contexts;
	// Use this for initialization
	void Start () {
        _contexts = Contexts.sharedInstance;
	}
	
	// Update is called once per frame
	void Update () {
        ProcessInput();
	}

    #region Private MEthods

    private void ProcessInput()
    {
        if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            Vector2 direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            ClientEntity inputEntity = _contexts.client.CreateEntity();
            inputEntity.AddDirection(direction);
        }
    }
    #endregion
}
