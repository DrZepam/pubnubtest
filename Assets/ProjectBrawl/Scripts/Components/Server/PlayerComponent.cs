﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using System;
[Client]
[Server]
[Serializable]
public class PlayerComponent : IComponent
{
    [PrimaryEntityIndex]
    public string PlayerId;
}
