﻿using System;
using UnityEngine;
using Entitas;

[Client][Server]
[Serializable]
public class DirectionComponent : IComponent {
    public Vector2 Direction;
}
