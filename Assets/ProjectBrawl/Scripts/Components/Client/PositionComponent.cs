﻿using System;
using UnityEngine;
using Entitas;

[Client][Server]
[Serializable]
public class PositionComponent : IComponent {
    public Vector3 Position;
}
