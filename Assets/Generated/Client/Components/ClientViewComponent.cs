//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ClientEntity {

    public ViewComponent view { get { return (ViewComponent)GetComponent(ClientComponentsLookup.View); } }
    public bool hasView { get { return HasComponent(ClientComponentsLookup.View); } }

    public void AddView(UnityEngine.GameObject newGameObject) {
        var index = ClientComponentsLookup.View;
        var component = (ViewComponent)CreateComponent(index, typeof(ViewComponent));
        component.GameObject = newGameObject;
        AddComponent(index, component);
    }

    public void ReplaceView(UnityEngine.GameObject newGameObject) {
        var index = ClientComponentsLookup.View;
        var component = (ViewComponent)CreateComponent(index, typeof(ViewComponent));
        component.GameObject = newGameObject;
        ReplaceComponent(index, component);
    }

    public void RemoveView() {
        RemoveComponent(ClientComponentsLookup.View);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ClientMatcher {

    static Entitas.IMatcher<ClientEntity> _matcherView;

    public static Entitas.IMatcher<ClientEntity> View {
        get {
            if (_matcherView == null) {
                var matcher = (Entitas.Matcher<ClientEntity>)Entitas.Matcher<ClientEntity>.AllOf(ClientComponentsLookup.View);
                matcher.componentNames = ClientComponentsLookup.componentNames;
                _matcherView = matcher;
            }

            return _matcherView;
        }
    }
}
