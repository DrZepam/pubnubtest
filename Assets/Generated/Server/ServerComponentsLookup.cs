//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class ServerComponentsLookup {

    public const int Direction = 0;
    public const int Dispose = 1;
    public const int Movable = 2;
    public const int Player = 3;
    public const int Position = 4;
    public const int RemoveViewOnDispose = 5;

    public const int TotalComponents = 6;

    public static readonly string[] componentNames = {
        "Direction",
        "Dispose",
        "Movable",
        "Player",
        "Position",
        "RemoveViewOnDispose"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(DirectionComponent),
        typeof(DisposeComponent),
        typeof(MovableComponent),
        typeof(PlayerComponent),
        typeof(PositionComponent),
        typeof(RemoveViewOnDispose)
    };
}
